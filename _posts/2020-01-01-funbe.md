---
layout: redirect
title: Redirect
description: ''
permalink: /funbe
lang: ko
lang_index: 0
meta:
  title: 펀비
  desc: 펀비(Funbe)는 네이버웹툰 다음웹툰 카카오웹툰 레진코믹스 짬툰 투믹스 탑툰 만화책 미리보기 및 다시보기를 제공합니다.
  link: 'https://olink.netlify.app/webtoon/funbe'
  img: '/assets/images/funbe-link.jpg'
  domain: funbe.com
---
